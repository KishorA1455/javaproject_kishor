package com.javaproject.JavaProject.DAO;

import com.javaproject.JavaProject.Entities.Payment;

import java.util.List;

public interface PaymentDAO {
    public int rowCount();

    public Payment findById(int id);

    public List<Payment> findByType(String type);

    public List<Payment> findAll();

    int save(Payment payment);
}
