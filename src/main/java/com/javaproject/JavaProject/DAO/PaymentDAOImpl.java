package com.javaproject.JavaProject.DAO;

import com.javaproject.JavaProject.Entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PaymentDAOImpl implements PaymentDAO {
    @Autowired
    MongoTemplate mpl;
    Query query;

    public PaymentDAOImpl() {
    }

    @Override
    public int rowCount() {
        query = new Query();
        return (int) mpl.count(query, Payment.class);
    }

    @Override
    public Payment findById(int id) {
        query = new Query();
        query.addCriteria(Criteria.where("_id").is(id));
        Payment p = mpl.findOne(query, Payment.class);
        return p;
    }

    @Override
    public List<Payment> findByType(String type) {
        query = new Query();
        query.addCriteria(Criteria.where("type").is(type));
        List<Payment> lp = mpl.find(query, Payment.class);
        return lp;
    }


    @Override
    public List<Payment> findAll() {
        query = new Query();
        List<Payment> lp = mpl.findAll(Payment.class);
        return lp;
    }

    @Override
    public int save(Payment payment) {
        try {
            Payment pt = mpl.save(payment);
            return 1;
        }

        catch(Exception ex){
            return 0;
        }
    }
}
