package com.javaproject.JavaProject.Entities;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.Date;

@Document
public class Payment {
    @Id
    private int id;
    private Date paymentDate;
    private String type;
    private double amount;
    private int custId;

    public Payment(int id, Date paymentDate, String type, double amount, int custId) {
        this.id = id;
        this.paymentDate = paymentDate;
        this.type = type;
        this.amount = amount;
        this.custId = custId;
    }

    public Payment() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        if (id < 1){
            //return ("id < 1");
        }
        this.id = id;
    }

    public Date getPaymentDate() {
        return paymentDate;
    }

    public void setPaymentDate(Date paymentDate) {
        this.paymentDate = paymentDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        if(type.isBlank() || type.isEmpty() || type.toString() == "" || type == null){
            //throw new Exception("Type shouldn't be blank");
        }
        this.type = type;
    }

    public double getamount() {
        return amount;
    }

    public void setamount(double amount) {
        if (amount < 1){
            //throw new Exception("Amount must be > 0");
        }
        this.amount = amount;
    }

    public int getCustId() {
        return custId;
    }

    public void setCustId(int custId){
        if (custId < 1){
            //throw new Exception("custId < 1");
        }
        this.custId = custId;
    }

    public String toString(){
        return ("Values are "+this.id + " - " + this.paymentDate + " - " + this.amount + " - " + this.type + " - " + this.custId);
        //return ("Values are "+this.id + " - " + %s %f %s %d", this.id, this.paymentDate, this.amount, this.type, this.custId);
    }

}
