package com.javaproject.JavaProject;

import com.javaproject.JavaProject.Entities.Payment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Date;

@SpringBootApplication
public class JavaProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaProjectApplication.class, args);
		Payment p = new Payment(2, new Date(), "Another", 5000, 2);
		System.out.printf(p.toString());
	}

}
