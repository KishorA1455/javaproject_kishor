package com.javaproject.JavaProject.Rest;

import com.javaproject.JavaProject.Entities.Payment;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

public interface PaymentController {

    @RequestMapping(value="/status", method = RequestMethod.GET)
    String status();

    @RequestMapping(value="/count", method = RequestMethod.GET)
    int rowCount();

    @RequestMapping(value="/find/{id}", method = RequestMethod.GET)
    ResponseEntity<Payment> findById(@PathVariable("id") int id);

    @RequestMapping(value="/find/{type}", method = RequestMethod.GET)
    List<Payment> findByType(@PathVariable("type") String type);

    @RequestMapping(value="/findall", method = RequestMethod.GET)
    List<Payment> findAll();

    @RequestMapping(value="/save", method = RequestMethod.POST)
    int save(@RequestBody Payment payment);
}
