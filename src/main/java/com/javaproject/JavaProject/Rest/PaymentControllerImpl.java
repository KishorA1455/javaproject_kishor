package com.javaproject.JavaProject.Rest;

import com.javaproject.JavaProject.Entities.Payment;
import com.javaproject.JavaProject.Services.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping("/paymentapi")
public class PaymentControllerImpl implements PaymentController{
    @Autowired
    PaymentService pS;

    public PaymentControllerImpl() {
    }

    @Override
    @RequestMapping(value="/status", method = RequestMethod.GET)
    public String status() {
        return "Payment REST is running";
    }

    @Override
    @RequestMapping(value="/count", method = RequestMethod.GET)
    public int rowCount() {
        int count = 0;
        try { count = pS.rowCount();
        }
        catch(Exception ex){
            System.out.printf("Error occurred in Payment API" + ex.toString());
        }
        return count;
    }

    @Override
    @RequestMapping(value="/find/{id}", method = RequestMethod.GET)
    public ResponseEntity<Payment> findById(@PathVariable("id") int id) {
        Payment payment = pS.findById(id);

        if (payment == null) {
            return new ResponseEntity<Payment>(HttpStatus.NOT_FOUND);
        }
        else {
            return new ResponseEntity<Payment>(payment, HttpStatus.OK);
        }
    }

    @Override
    @RequestMapping(value="/find/{type}", method = RequestMethod.GET)
    public List<Payment> findByType(@PathVariable("type") String type) {
        return pS.findByType(type);
    }

    @Override
    @RequestMapping(value="/findall", method = RequestMethod.GET)
    public List<Payment> findAll() {
        return pS.findAll(); //sampel comment
    }

    @Override
    @RequestMapping(value="/save", method = RequestMethod.POST)
    public int save(@RequestBody Payment payment) {
        return pS.save(payment);
    }
}
