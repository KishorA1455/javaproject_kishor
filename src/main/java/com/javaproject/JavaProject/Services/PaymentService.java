package com.javaproject.JavaProject.Services;

import com.javaproject.JavaProject.Entities.Payment;

import java.util.List;

public interface PaymentService {
    public int rowCount();

    public Payment findById(int id);

    public List<Payment> findByType(String type);

    public List<Payment> findAll();

    public int save(Payment payment);
}
