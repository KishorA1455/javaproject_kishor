package com.javaproject.JavaProject.Services;

import com.javaproject.JavaProject.DAO.PaymentDAO;
import com.javaproject.JavaProject.Entities.Payment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.Query;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Service
public class PaymentServiceImpl implements PaymentService{
    @Autowired
    PaymentDAO payD;
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    LocalDateTime now = LocalDateTime.now();
    Date date2 = (new Date(dateFormat.format(now)));

    public PaymentServiceImpl() {
    }

    @Override
    public int rowCount() {
        try{
            return payD.rowCount();
        }
        catch (Exception ex){
            throw ex;
        }
    }

    @Override
    public Payment findById(int id) {
        Payment p = null;
        try {
            if (id > 0)
                p = payD.findById(id);
            else { NullPointerException ex; }
        }
        catch (NullPointerException ex) {
            System.out.println("Error caught : id is not greater than zero");
        }
        catch (Exception ex){
            System.out.println("Error caught: Generic error caught ");
        }
        return p;
    }

    @Override
    public List<Payment> findByType(String type) {
        List<Payment> lp = null;
        try {
            if (type != null && type != "") {
                lp = payD.findByType(type);
            } else {NullPointerException ex;}
        }
        catch (NullPointerException nx) {
            System.out.println("Error caught: type value is null ");
        }
        catch (Exception ex){
            System.out.println("Error caught: Generic error caught ");
        }
        return lp;
    }

    @Override
    public List<Payment> findAll() {
        List<Payment> lp = null;
        lp = payD.findAll();
        return lp;
    }

    @Override
    public int save(Payment payment) {
        payment.setPaymentDate(date2);
        return payD.save(payment);
    }
}
