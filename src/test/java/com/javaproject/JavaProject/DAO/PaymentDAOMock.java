package com.javaproject.JavaProject.DAO;

import com.javaproject.JavaProject.Entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.AssertionErrors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.springframework.test.util.AssertionErrors.assertTrue;

public class PaymentDAOMock {
    @Mock
    PaymentDAO pDao;

    @BeforeEach
    void setUp(){
        initMocks(this);
    }

    @Test
    void rowCount(){
        doReturn(1).when(pDao).rowCount();
        assertEquals(1, pDao.rowCount());
    }

    @Test
    void savePayment_passed(){
        doReturn(0).when(pDao).save(null);
        assertEquals(0, pDao.save(null));
    }

    @Test
    void findByType() {
        List<Payment> payments = new ArrayList<Payment>();
        Payment p = new Payment(12, new Date(), "Saving", 1000, 2);
        payments.add(p);
        doReturn(payments).when(pDao).findByType("");
        AssertionErrors.assertEquals("Payment obj is matching", 1, payments.size());
    }

    @Test
    void findById(){
        Payment p = new Payment(12, new Date(), "Saving", 1000, 2);
        doReturn(p).when(pDao).findById(12);
        AssertionErrors.assertEquals("Matching", 1000.0, p.getamount());
    }
}
