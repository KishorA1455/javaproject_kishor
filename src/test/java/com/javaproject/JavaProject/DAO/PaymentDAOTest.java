package com.javaproject.JavaProject.DAO;

import com.javaproject.JavaProject.DAO.PaymentDAO;
import com.javaproject.JavaProject.Entities.Payment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.index.GeoSpatialIndexed;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class PaymentDAOTest {

    @Autowired
    private PaymentDAO pd;
    private MongoTemplate mongoTemplate;

    /*
    @BeforeAll
    void initiate() {
        Payment payment = new Payment(12, new Date(), "Saving", 1000, 2);
        int isPayment = pd.save(payment);
    }

    @AfterAll
    void tearDown() {
        if( mongoTemplate.getCollectionName(Payment.class) != null){
            String collectionName = mongoTemplate.getCollectionName(Payment.class);
            mongoTemplate.dropCollection(collectionName);
        }
    }
    */

    @Test
    void save_success() {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        Date date = new Date();
        Date date2 = (new Date(dateFormat.format(date)));
        Payment p = new Payment(14, date2, "Saving", 6000, 15);
        int isSaved = pd.save(p);
        assertEquals(1, isSaved);
    }

    @Test
    void rowCount() {
        Payment pDobj = new Payment();
        int paymentCount = pd.rowCount();
        assertEquals(1, paymentCount);
    }

    @Test
    void findById() {
        Payment pDobj = pd.findById(14);
        assertEquals(6000.0, pDobj.getamount());
    }

    @Test
    void findByType() {
        List<Payment> payments = pd.findByType("Saving");
        assertEquals(1, payments.size());
    }
}