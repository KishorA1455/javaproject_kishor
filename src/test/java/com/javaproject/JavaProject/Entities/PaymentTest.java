package com.javaproject.JavaProject.Entities;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PaymentTest {

    private Payment payment;
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    Date date = new Date();
    private Date date2 = (new Date(dateFormat.format(date)));

    @BeforeEach
    void setUp() {
        payment = new Payment(12,date2, "Saving",1000, 2);
    }

    @Test
    void getId() {
        assertEquals(12, payment.getId());
    }

    @Test
    void getType() {
        assertEquals("Saving", payment.getType());
    }

    @Test
    void getAmount() {
        assertEquals(1000.00, payment.getamount());
    }

    @Test
    void getCustomerId() { assertEquals(2, payment.getCustId());
    }

}