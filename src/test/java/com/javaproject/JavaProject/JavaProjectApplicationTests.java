package com.javaproject.JavaProject;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.RestTemplate;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest (webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ExtendWith(SpringExtension.class)
class JavaProjectApplicationTests {

	int port = 8080;

	@Test
	void contextLoads() {}

	@Test
	public void testAPIreturnMessage() {
		RestTemplate restTemplate = new RestTemplate();
		assertThat(restTemplate.getForObject("http://localhost:" + port + "/paymentapi/status", String.class))
				.contains("Payment REST is running");
	}
}
