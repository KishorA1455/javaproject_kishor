package com.javaproject.JavaProject.Services;

import com.javaproject.JavaProject.Entities.Payment;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.test.util.AssertionErrors;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.MockitoAnnotations.initMocks;

class PaymentServiceMock {

    @Mock
    PaymentService iPaymentService;

    @BeforeEach
    void setUp(){
        initMocks(this);
    }

    @Test
    void rowCount() {
        doReturn(2).when(iPaymentService).rowCount();
        assertEquals(2, iPaymentService.rowCount());
    }

    @Test
    void findById() {
        Payment p = new Payment(12, new Date(), "Saving", 1000, 2);
        doReturn(p).when(iPaymentService).findById(12);
        AssertionErrors.assertEquals("Object is matching", 1000.0, p.getamount());
    }

    @Test
    void findByType() {
        List<Payment> payments = new ArrayList<Payment>();
        Payment p = new Payment(12, new Date(), "Saving", 1000, 2);
        payments.add(p);
        doReturn(payments).when(iPaymentService).findByType("");
        AssertionErrors.assertEquals("Matching", 1, payments.size());
    }

    @Test
    void save() {
        doReturn(0).when(iPaymentService).save(null);
        assertEquals (0, iPaymentService.save(null));
    }
}