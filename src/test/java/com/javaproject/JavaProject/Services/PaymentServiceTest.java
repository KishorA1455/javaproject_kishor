package com.javaproject.JavaProject.Services;

import com.javaproject.JavaProject.Entities.Payment;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoTemplate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.util.AssertionErrors.assertTrue;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)

public class PaymentServiceTest {

    @Autowired
    PaymentService iPaymentService;

    @Autowired
    MongoTemplate mongoTemplate;

    @BeforeAll
    void initiate(){
        Payment p = new Payment(12, new Date(), "Saving", 1000, 2);
        int isPaymentSaved = iPaymentService.save(p);
    }

    @Test
    void savePayment() {
        Payment p = new Payment(12, new Date(), "Saving", 1000, 2);
        int isPaymentSaved = iPaymentService.save(p);
        assertEquals("Payment saved successfully", 1, isPaymentSaved);
    }

    @Test
    void rowCount() {
        int count = iPaymentService.rowCount();
        assertTrue("Payment count",count>0);
    }

    @Test
    void findByType() {
        List<Payment> payments = iPaymentService.findByType("Saving");
        assertEquals("Payment object is matching", 2, payments.size());
    }

    @Test
    void findByType_false() {
        List<Payment> payments = iPaymentService.findByType("C");
        assertEquals("Payment object is matching", 0, payments.size());
    }

    @Test
    void findById() {
        String pattern = ("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        Payment payment = iPaymentService.findById(12);
        String date = simpleDateFormat.format(payment.getPaymentDate());
        assertEquals("Payment object is matching", 1000.0, payment.getamount());
        assertEquals("Check the date format", date, simpleDateFormat.format(payment.getPaymentDate()));
    }

    @AfterAll
    void  tearDown() {
        if( mongoTemplate.getCollectionName(Payment.class) != null) {
            String collectionName = mongoTemplate.getCollectionName(Payment.class);
            mongoTemplate.dropCollection(collectionName);
        }
    }
}